ModelPager = function(name) {
    this.name = name;
    this.URL = "http://radiobox2.omroep.nl/documentation/pager/" + this.name + "/";
}

ModelPager.prototype.getTotalPages = function() {
    return jQuery("table:last tr td:eq(1) a").length;
};

ModelPager.prototype.getCurrentPageModels = function() {
    var _data = [];
    jQuery("table:first tr:not(:first)").each(function(index, tr) {
        _data.push({
            id: parseInt(jQuery(tr).find("td:first").text()),
            name: jQuery(tr).find("td:last").text(),
        });
    });
    return _data;
};

ModelPager.prototype.removeArrayBrackets = function(items) {
    return JSON.stringify(items).replace(/[\[|\]]+/mg, "");
};

module.exports = ModelPager;