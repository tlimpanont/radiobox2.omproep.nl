var path = require('path');
var childProcess = require('child_process');
var fs = require("fs");
var childArgs = [
    path.join(__dirname, 'casperjs.channel.js')
];
var https = require("https");
childProcess.execFile("/usr/bin/casperjs", childArgs, function(err, stdout, stderr) {
    var data = eval("[" + stdout + "]");
    fs.writeFile("data/channel-list.json", JSON.stringify(data, null, 4), function() {

    });

    var req = https.request({
        hostname: "radiobox2-omroep-nl.firebaseio.com",
        method: "PUT",
        path: "/channels.json"
    });

    req.end(JSON.stringify(data));

    console.log('saved into Firebase DB');
});