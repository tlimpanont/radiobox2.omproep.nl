var Pager = require("./ModelPager");
var pager = new Pager("Channel");

var casper = require('casper').create({
    clientScripts: [
        "../libs/jquery-2.1.3.min.js",
        "../libs/lodash.min.js",
    ]
});


casper.start(pager.URL, function() {

    var totalPages = this.evaluate(pager.getTotalPages);

    var urls = [];
    
    for (var i = 1; i <= totalPages; i++) {
        urls.push(pager.URL + i);
    }

    casper.start().eachThen(urls, function(response) {
        this.thenOpen(response.data, function(response) {
            var items = casper.evaluate(pager.getCurrentPageModels);
            casper.emit('broadcaster.page.loaded', pager.removeArrayBrackets(items));
        });
        
    });

    casper.on('broadcaster.page.loaded', function(data) {
        this.echo(data + ",");
    });
});



casper.run();