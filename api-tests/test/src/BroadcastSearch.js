(function(window, moment, _, jQuery) {
    BroadcastSearch = function(options) {
    	if(options === undefined) 
    		throw new Error("You have to specify the broadcast search model");

        this.apiURL = "http://radiobox2.omroep.nl/broadcast/search.json";
        this.apiDatetimeFormat = "YYYY-MM-DD HH:mm:ss";
        this.options = options;
    }

    BroadcastSearch.prototype.getTodayMoment = function() {
        var format = moment().format("YYYY-MM-DD");
        return moment(format, this.apiDatetimeFormat);
    };

    BroadcastSearch.prototype.offsetStartDatetime = function(momentDatetime) {
        return momentDatetime.clone()
            .subtract(1, 'minutes')
            .format(this.apiDatetimeFormat);
    };

    BroadcastSearch.prototype.offsetEndDatetime = function(momentDatetime) {
        return momentDatetime.clone()
            .add(1, "minutes")
            .add(1, "days")
            .format(this.apiDatetimeFormat);
    };

    BroadcastSearch.prototype.getBroadcastOfTodaysMoment = function(apiDatetime, callback) {
        var todayMoment = this.getTodayMoment();
        var startdatetime = this.offsetStartDatetime(todayMoment);
        var stopdatetime = this.offsetEndDatetime(todayMoment);

        var momentDatetime = moment(apiDatetime, this.apiDatetimeFormat);

        try{
        	var q = "?q=broadcaster.id:'" + this.options.model.broadcaster.id + "'";
        } catch(e){
        	var q = "?q=channel.id:'" + this.options.model.channel.id + "'";
        }
        
        jQuery.get(this.apiURL + q +" AND startdatetime>'" + startdatetime + "' AND stopdatetime<'" + stopdatetime + "'&order=startdatetime:asc").done(function(data) {

            var suggestedBroadcast = _.filter(data.results, function(broadcast) {

                var range = moment().range(
                    moment(new Date(broadcast.startdatetime)),
                    moment(new Date(broadcast.stopdatetime))
                );
                return range.contains(momentDatetime);
            })[0];

            callback.call(this, suggestedBroadcast);

        }.bind(this));
    };

    BroadcastSearch.prototype.getCurrentBroadcast = function(callback) {
        BroadcastSearch.prototype.getBroadcastOfTodaysMoment.call(this, moment().format(this.apiDatetimeFormat), callback);
    };


})(window, moment, _, jQuery);