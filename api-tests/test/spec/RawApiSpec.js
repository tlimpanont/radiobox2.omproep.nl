/* global describe, it */

var mysqlDateTimeFormat = "YYYY-MM-DD HH:mm:ss";
var _26Dec = moment("2014-12-26 00:00:00", mysqlDateTimeFormat);
var _24Dec = moment("2014-12-24 00:00:00", mysqlDateTimeFormat);
var _25Dec = moment("2014-12-25 00:00:00", mysqlDateTimeFormat);


function offsetStartDatetime(momentDateTime) {
    return momentDateTime.clone()
        .subtract(1, 'minutes')
        .format(mysqlDateTimeFormat);
}

function offsetEndDatetime(momentDateTime) {
    return momentDateTime.clone()
        .add(1, "minutes")
        .add(1, "days")
        .format(mysqlDateTimeFormat);
}

(function(window, moment, jQuery, _) {

    describe('radiobox datetime calculation and creation', function() {

        it("should calculate the right offset stardatetime for 26 Dec 2014", function() {
            expect(offsetStartDatetime(_26Dec)).to.equal("2014-12-25 23:59:00");
        });

        it("should calculate the right offset enddatetime for 26 Dec 2014", function() {
            expect(offsetEndDatetime(_26Dec)).to.equal("2014-12-27 00:01:00");
        });

        it("should calculate the right offset stardatetime for 24 Dec 2014", function() {
            expect(offsetStartDatetime(_24Dec)).to.equal("2014-12-23 23:59:00");
        });

        it("should calculate the right offset enddatetime for 24 Dec 2014", function() {
            expect(offsetEndDatetime(_24Dec)).to.equal("2014-12-25 00:01:00");
        });

        it("should calculate the right offset stardatetime for 25 Dec 2014", function() {
            expect(offsetStartDatetime(_25Dec)).to.equal("2014-12-24 23:59:00");
        });

        it("should calculate the right offset enddatetime for 25 Dec 2014", function() {
            expect(offsetEndDatetime(_24Dec)).to.equal("2014-12-25 00:01:00");
        });
    });


    describe('radiobox2.omroep.nl API tests', function() {
        var broadcastURL;


        beforeEach(function() {
            broadcastURL = "http://radiobox2.omroep.nl/broadcast/search.json";
        });

        describe('3FM Channel', function() {

            it('should test that there is 8 broadcast items on 26 Dec 2014', function(done) {

                var startDatetime = offsetStartDatetime(_26Dec);
                var endDatetime = offsetEndDatetime(_26Dec);

                jQuery.get(broadcastURL + "?q=channel.id:'3' AND startdatetime>'" + startDatetime + "' AND stopdatetime<'" + endDatetime + "'&order=startdatetime:asc").done(function(data) {
                    expect(data["total-results"]).to.equal(8);
                    done();
                });
            });

            it('should test that there is 7 broadcast items on 24 Dec 2014', function(done) {

                var startDatetime = offsetStartDatetime(_24Dec);
                var endDatetime = offsetEndDatetime(_24Dec);

                jQuery.get(broadcastURL + "?q=channel.id:'3' AND startdatetime>'" + startDatetime + "' AND stopdatetime<'" + endDatetime + "'&order=startdatetime:asc").done(function(data) {
                    expect(data["total-results"]).to.equal(7);
                    done();
                });
            });

            it('should test that there is 7 broadcast items on 25 Dec 2014', function(done) {

                var startDatetime = offsetStartDatetime(_24Dec);
                var endDatetime = offsetEndDatetime(_24Dec);

                jQuery.get(broadcastURL + "?q=channel.id:'3' AND startdatetime>'" + startDatetime + "' AND stopdatetime<'" + endDatetime + "'&order=startdatetime:asc").done(function(data) {
                    expect(data["total-results"]).to.equal(7);
                    done();
                });
            });

            it('should display the first broadcast of 26 Dec correctly', function(done) {

                jQuery.get(broadcastURL + "?q=channel.id:'3' AND startdatetime:'2014-12-26'&order=startdatetime:asc").done(function(data) {
                    expect(data.results[0].presenter[0].firstname).to.equal("Joram");
                    expect(data.results[0].presenter[0].lastname).to.equal("Kaat");
                    done();
                });
            });

            it('should return the Top Serious Request Broadcast with Frank van der Lende as presenter on 26 Dec @23:45 hr', function(done) {
                var startDatetime = offsetStartDatetime(_26Dec);
                var endDatetime = offsetEndDatetime(_26Dec);

                jQuery.get(broadcastURL + "?q=channel.id:'3' AND startdatetime>'" + startDatetime + "' AND stopdatetime<'" + endDatetime + "'&order=startdatetime:asc").done(function(data) {

                    var latestBroadcast = _.last(data.results);

                    expect(latestBroadcast.name).to.equal("Top Serious Request");
                    expect(latestBroadcast.presenter[0].full_name).to.equal("Frank van der Lende");
                    expect(moment(new Date(latestBroadcast.startdatetime)).format("YYYY-MM-DD HH:mm:ss")).to.equal("2014-12-26 21:00:00");
                    expect(moment(new Date(latestBroadcast.stopdatetime)).format("YYYY-MM-DD HH:mm:ss")).to.equal("2014-12-27 00:00:00");

                    done();
                });
            });

            it('should select the correct broadcast from an arbitary datetime moment of 26 Dec', function(done) {
                var startDatetime = offsetStartDatetime(_26Dec);
                var endDatetime = offsetEndDatetime(_26Dec);

                jQuery.get(broadcastURL + "?q=channel.id:'3' AND startdatetime>'" + startDatetime + "' AND stopdatetime<'" + endDatetime + "'&order=startdatetime:asc").done(function(data) {

                    var _arbitaryDateTime = moment("2014-12-26 12:45:20", mysqlDateTimeFormat);

                    var suggestedBroadcast = _.filter(data.results, function(broadcast) {

                        var range = moment().range(
                            moment(new Date(broadcast.startdatetime)),
                            moment(new Date(broadcast.stopdatetime))
                        );
                        return range.contains(_arbitaryDateTime);
                    })[0];

                    expect(suggestedBroadcast.name).to.equal("Top Serious Request");
                    expect(suggestedBroadcast.presenter[0].full_name).to.equal("Paul Rabbering");


                    var _arbitaryDateTime = moment("2014-12-26 10:11:20", mysqlDateTimeFormat);

                    var suggestedBroadcast = _.filter(data.results, function(broadcast) {

                        var range = moment().range(
                            moment(new Date(broadcast.startdatetime)),
                            moment(new Date(broadcast.stopdatetime))
                        );
                        return range.contains(_arbitaryDateTime);
                    })[0];

                    expect(suggestedBroadcast.name).to.equal("Top Serious Request");
                    expect(suggestedBroadcast.presenter[0].full_name).to.equal("Michiel Veenstra");

                    done();
                });
            });

            it('should select the correct broadcast from an arbitary datetime moment of 24 Dec', function(done) {
                var startDatetime = offsetStartDatetime(_24Dec);
                var endDatetime = offsetEndDatetime(_24Dec);

                jQuery.get(broadcastURL + "?q=channel.id:'3' AND startdatetime>'" + startDatetime + "' AND stopdatetime<'" + endDatetime + "'&order=startdatetime:asc").done(function(data) {

                    var _arbitaryDateTime = moment("2014-12-24 07:58:00", mysqlDateTimeFormat);

                    var suggestedBroadcast = _.filter(data.results, function(broadcast) {

                        var range = moment().range(
                            moment(new Date(broadcast.startdatetime)),
                            moment(new Date(broadcast.stopdatetime))
                        );
                        return range.contains(_arbitaryDateTime);
                    })[0];

                    expect(suggestedBroadcast.name).to.equal("3FM Serious Request");
                    expect(suggestedBroadcast.presenter[0].full_name).to.equal("Gerard Ekdom");

                    done();
                });
            });

        });

    });


})(window, moment, jQuery, _);