describe("BroadcastSearch API", function() {

    describe("Radio ChannelID 3 and BroadcasterID 30 = 3FM", function() {
        
        var search = new BroadcastSearch({
            model: {
                channel: {
                    id: 3
                }
            }
        });

        var _26Dec = moment("2014-12-26 00:00:00", search.apiDatetimeFormat);
        var _24Dec = moment("2014-12-24 00:00:00", search.apiDatetimeFormat);
        var _25Dec = moment("2014-12-25 00:00:00", search.apiDatetimeFormat);

        beforeEach(function() {

        });

        it("should calculate the right offset stardatetime for 26 Dec 2014", function() {
            expect(search.offsetStartDatetime(_26Dec)).to.equal("2014-12-25 23:59:00");
        });

        it("should calculate the right offset enddatetime for 26 Dec 2014", function() {
            expect(search.offsetEndDatetime(_26Dec)).to.equal("2014-12-27 00:01:00");
        });

        it("should calculate the right offset stardatetime for 24 Dec 2014", function() {
            expect(search.offsetStartDatetime(_24Dec)).to.equal("2014-12-23 23:59:00");
        });

        it("should calculate the right offset enddatetime for 24 Dec 2014", function() {
            expect(search.offsetEndDatetime(_24Dec)).to.equal("2014-12-25 00:01:00");
        });

        it("should calculate the right offset stardatetime for 25 Dec 2014", function() {
            expect(search.offsetStartDatetime(_25Dec)).to.equal("2014-12-24 23:59:00");
        });

        it("should calculate the right offset enddatetime for 25 Dec 2014", function() {
            expect(search.offsetEndDatetime(_24Dec)).to.equal("2014-12-25 00:01:00");
        });

        it("should get corrent today moment date", function() {
            expect(search.getTodayMoment().format("YYYY-MM-DD"))
                .to.equal("2014-12-27");
        });

        it("should get the correct broadcast on 2014-12-27 19:10:25", function(done) {
            search.getBroadcastOfTodaysMoment("2014-12-27 19:10:25", function(broadcast) {
                expect(broadcast.name).to.equal("Top Serious Request");
                expect(broadcast.presenter[0].full_name).to.equal("Sander Hoogendoorn");
                done();
            });
        });

        it("should get the correct broadcast on 2014-12-27 21:15:25", function(done) {
            search.getBroadcastOfTodaysMoment("2014-12-27 21:15:25", function(broadcast) {
                expect(broadcast.name).to.equal("Top Serious Request");
                expect(broadcast.presenter[0].full_name).to.equal("Frank van der Lende");
                done();
            });
        });

        it("should get the correct broadcast with broadcaster.id = 30, on 2014-12-27 21:15:25", function(done) {
            var search = new BroadcastSearch({
                model: {
                    broadcaster: {
                        id: 30
                    }
                }
            });
            search.getBroadcastOfTodaysMoment("2014-12-27 21:15:25", function(broadcast) {
                expect(broadcast.name).to.equal("Top Serious Request");
                expect(broadcast.presenter[0].full_name).to.equal("Frank van der Lende");
                done();
            });
        });


        xit("should get the correct broadcast on @current todays moment", function(done) {
            search.getCurrentBroadcast(function(broadcast) {
                expect(broadcast.name).to.equal("Top Serious Request");
                expect(broadcast.presenter[0].full_name).to.equal("Paul Rabbering");
                done();
            });
        });
    });

    describe("Radio ChannelID 1 = NPO Radio 1", function() {
        var search = new BroadcastSearch({
            model: {
                channel: {
                    id: 1
                }
            }
        });

        it("should get the correct broadcast on 2014-12-27 08:10:55", function(done) {
            search.getBroadcastOfTodaysMoment("2014-12-27 08:10:55", function(broadcast) {
                expect(broadcast.name.toLowerCase()).to.equal("NOS-RADIO 1-JOURNAAL".toLowerCase());
                expect(broadcast.presenter).to.be.undefined;
                done();
            });
        });

        xit("should get the correct current broadcast", function(done) {
            search.getCurrentBroadcast(function(broadcast) {
                expect(broadcast.name.toLowerCase()).to.equal("de taalstaat");
                expect(broadcast.presenter).to.be.undefined;
                done();
            });
        });
    });
});